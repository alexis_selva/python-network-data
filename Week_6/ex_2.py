import urllib.request, urllib.parse, urllib.error
import json

# Specify URL of the service
serviceurl = 'http://py4e-data.dr-chuck.net/geojson?'

while True:
    address = input('Enter location: ')
    if len(address) < 1: break

    # Concatenat URL with encoded location
    url = serviceurl + urllib.parse.urlencode(
        {'address': address})

    print('Retrieving', url)
    uh = urllib.request.urlopen(url)
    data = uh.read().decode()
    print('Retrieved', len(data), 'characters')

    # Load JSON data
    try:
        js = json.loads(data)
    except:
        js = None

    # Check error
    if not js or 'status' not in js or js['status'] != 'OK':
        print('==== Failure To Retrieve ====')
        print(data)
        continue

    # Print place id
    place_id = js["results"][0]["place_id"]
    print('place_id', place_id)
