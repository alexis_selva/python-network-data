import urllib.request, urllib.parse, urllib.error
import json

# Request URL
url = input('Enter - ')
uh = urllib.request.urlopen(url)
data = uh.read()

# Retrieve count
info = json.loads(data.decode())

# Sum count
sum_count = 0
for item in info['comments']:
    sum_count += int(item['count'])

# Print sum
print(sum_count)
