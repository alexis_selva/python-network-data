from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# Request URL
url = input('Enter - ')
html = urlopen(url, context=ctx).read()

# Retrieve all of the anchor tags
sum_span = 0
soup = BeautifulSoup(html, "html.parser")
tags = soup('span')
for tag in tags:
    sum_span += int(tag.contents[0])

# Print sum
print(sum_span)
