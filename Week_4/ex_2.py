import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# Read inputs
url = input('URL - ')
position_ = input('Position - ')
position = int(position_) - 1
repetition_ = input('Repetition - ')
repetition = int(repetition_)

# Request url
for lap in range(repetition):
    html = urllib.request.urlopen(url, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')
    # Retrieve all of the anchor tags
    tags = soup('a')
    tag = tags[position]
    url = tag.get('href', None)
    print(tag.contents[0])
