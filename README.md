These are the programming assignments relative to Python for everybody - network data (3rd part)

- Week 2: Regular Expressions (Chapter 11)
- Week 3: Networks and Sockets (Chapter 12)
- Week 4: Programs that Surf the Web (Chapter 12)
- Week 5: Web Services and XML (Chapter 13)
- Week 6: JSON and the REST Architecture (Chapter 13)

For more information, I invite you to have a look at https://www.coursera.org/learn/python-network-data
