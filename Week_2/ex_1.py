import re

# Read the file
name = input('Enter file:')
try:
    handle = open(name)
except:
    print('Error while reading', name)
    quit()

# Look for the integers
sum_integers = 0
for line in handle:
    line = line.rstrip()
    patterns = re.findall('[0-9]+', line)

    # Sum the integers
    for integer in patterns:
        sum_integers += int(integer)

# Print the sum of the integers
print(sum_integers)
