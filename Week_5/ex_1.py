import urllib.request, urllib.parse, urllib.error
import xml.etree.ElementTree as ET

# Request URL
url = input('Enter - ')
uh = urllib.request.urlopen(url)
data = uh.read()

# Retrieve count
tree = ET.fromstring(data)
counts = tree.findall('comments/comment/count')

# Sum count
sum_count = 0
for count in counts:
    sum_count += int(count.text)

# Print sum
print(sum_count)
